Feature: validate search with search Type "EPP_Search by_Customer Last Name"


@EPP_Searchby_CustomerLastName @testme @all
Scenario: Validating search functionality 
Given I am in EPP url with "EPP_Search by_Customer Last Name"
When I login to the application
Then I click to the search button
Then I select search type
Then Validate searched result
Then  I logout