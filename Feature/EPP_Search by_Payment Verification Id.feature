Feature: validate search with search Type "EPP_Search by_Payment Verification Id"


@EPP_Searchby_PaymentVerificationId @testme @all
Scenario: Validating search functionality 
Given I am in EPP url with "EPP_Search by_Payment Verification Id"
When I login to the application
Then I click to the search button
Then I select search type
Then  I logout