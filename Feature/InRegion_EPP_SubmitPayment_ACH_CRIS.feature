Feature: validate login with Transaction Type "InRegion_EPP_SubmitPayment_ACH_CRIS"

@InRegion_EPP_SubmitPayment_ACH_CRIS @all
Scenario: Validating login functionality with CRIS and ACH
Given I am in EPP url with "InRegion_EPP_SubmitPayment_ACH_CRIS"
When I login to the application
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout