Feature: validate login with Transaction Type "EPP_Multipayment_ACH"
@EPP_Multipayment_ACH @all
Scenario: Validating login functionality with MULTIPAYMENT and ACH
Given I am in EPP url with "EPP_Multipayment_ACH"
When I login to the application
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number for multipayment
Then I logout