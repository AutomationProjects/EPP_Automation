Feature: validate login with Transaction Type "OutOfRegion_EPP_SubmitPayment_CARD_LATIS"


@OutOfRegion_EPP_SubmitPayment_CARD_LATIS @all
Scenario: Validating login functionality with LATIS and CARD
Given I am in EPP url with "OutOfRegion_EPP_SubmitPayment_CARD_LATIS"
When I login to the application
Then I click on Out Region
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout
