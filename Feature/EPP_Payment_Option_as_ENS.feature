Feature: validate the payment option with Billing Application System as "EPP_Payment_Option_as_ENS"

@Payment @EPP_Payment_Option_as_ENS
Scenario: Validation of payment option 
Given I am in EPP url with "EPP_Payment_Option_as_ENS"
When I login to the application
Then I click to the Payment option button
Then I enter Billing Application Account Id

#Then  I logout
