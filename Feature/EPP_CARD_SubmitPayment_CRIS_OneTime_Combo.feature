Feature: validate login with Transaction Type "EPP_CARD_SubmitPayment_CRIS_OneTime_Combo"

@EPP_CARD_SubmitPayment_CRIS_OneTime_Combo @all
Scenario: Validating Submit Payment with CRIS Combo BTN using CARD
Given I am in EPP url with "EPP_CARD_SubmitPayment_CRIS_OneTime_Combo"
When I login to the application
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout