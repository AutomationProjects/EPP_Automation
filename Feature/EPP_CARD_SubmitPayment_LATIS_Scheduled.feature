
Feature: validate login with Transaction Type "EPP_CARD_SubmitPayment_LATIS_Scheduled"

@EPP_CARD_SubmitPayment_LATIS_Scheduled
Scenario: Validating  scheduled payment functionality with BART using ACH
Given I am in EPP url with "EPP_CARD_SubmitPayment_LATIS_Scheduled"
When I login to the application
Then I click on Out Region
Then I enter accountNumber
Then I select Transaction type
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout