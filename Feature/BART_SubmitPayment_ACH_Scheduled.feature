
Feature: validate login with Transaction Type "BART_SubmitPayment_ACH_Scheduled"

@BART_SubmitPayment_ACH @Kusum @all
Scenario: Validating  scheduled payment functionality with BART using ACH
Given I am in EPP url with "BART_SubmitPayment_ACH_Scheduled"
When I login to the application
Then I click on Bart
Then I enter accountNumber
Then I select Transaction type
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout