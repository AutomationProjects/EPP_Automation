Feature: validate login with Transaction Type "BART_MultiInvoice_ACH"

  @BART_MultiInvoice_ACH @Kusum
  Scenario: Validating  MultiInvoice payment functionality with BART using ACH
    Given I am in EPP url with "BART_MultiInvoice_ACH"
    When I login to the application
    Then I click on Bart
    Then I enter accountNumber
    Then I select Transaction type
    And I fill all mandatory fields
    Then fill NegotiatePayment fields
    Then I verify confirmation Number for multipayment
    Then I logout
