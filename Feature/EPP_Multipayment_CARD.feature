Feature: validate login with Transaction Type "EPP_Multipayment_CARD"


@EPP_Multipayment_CARD @all
Scenario: Validating login functionality with MULTIPAYMENT and CARD
Given I am in EPP url with "EPP_Multipayment_CARD"
When I login to the application
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number for multipayment
Then I logout

