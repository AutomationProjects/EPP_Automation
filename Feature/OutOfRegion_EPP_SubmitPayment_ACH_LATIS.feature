Feature: validate login with Transaction Type "OutOfRegion_EPP_SubmitPayment_ACH_LATIS"


@OutOfRegion_EPP_SubmitPayment_ACH_LATIS @all
Scenario: Validating login functionality with LATIS and ACH
Given I am in EPP url with "OutOfRegion_EPP_SubmitPayment_ACH_LATIS"
When I login to the application
Then I click on Out Region
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout
