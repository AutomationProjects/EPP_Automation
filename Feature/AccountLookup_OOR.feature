Feature: validate login with Transaction Type "AccountLookup_OOR"

@AccountLookup_OOR @all
Scenario: Validating account lookup functionality with LATIS
Given I am in EPP url with "AccountLookup_OOR"
When I login to the application
Then I click on Out Region
Then I enter accountNumber
Then I select Transaction type
Then I validate whether account lookup is successful
Then I logout