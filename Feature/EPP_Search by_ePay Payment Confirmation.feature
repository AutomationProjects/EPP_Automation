Feature: validate search with search Type "EPP_Search by_ePay Payment Confirmation"


@EPP_Searchby_ePayPaymentConfirmation @testme
Scenario: Validating search functionality 
Given I am in EPP url with "EPP_Search by_ePay Payment Confirmation"

When I login to the application
Then I click to the search button
Then I select search type
Then  I logout