Feature: Validating Account lookup functionality with "New Service Deposit" as transaction type


@InRegion_NewServiceDeposit
Scenario: Validating Account lookup functionality with "New Service Deposit" as transaction type. User should able to do payment with new service deposit transaction
Given I am in EPP url with "InRegion_New Service Deposit"
When I login to the application
Then I enter accountNumber
Then I select Transaction type 
And I fill all mandatory fields for new service deposit
Then fill NegotiatePayment fields
Then I verify confirmation Number
Then I logout
