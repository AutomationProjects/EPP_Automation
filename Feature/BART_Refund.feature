Feature: validate login with Transaction Type "BART_Refund"

@BART_Refund @Kusum
Scenario: Validating  BART ACH refund
Given I am in EPP url with "BART_Refund"
When I login to the application
Then I click to the search button
Then I select search type
Then I verify PaymentID
Then I click on the payment_id
Then I click on the Refund_Btn
Then I verify refund_id