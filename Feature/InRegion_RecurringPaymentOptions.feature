Feature: Validate recurring payment Un-enrollment with Transaction Type "InRegion_RecurringPaymentOptions"


@InRegion_RecurringPaymentOptions
Scenario: Validate recurring payment Un-enrollment functionality for CRIS
Given I am in EPP url with "InRegion_RecurringPaymentOptions"
When I login to the application
Then I enter accountNumber
Then I select Transaction type
Then I click on Unenrollment radio button 
Then I click on Automatic Payment Unenrollment button
#And I fill all the mandatory fields
#Then fill NegotiatePayment fields
#Then I verify confirmation Number
Then I logout

