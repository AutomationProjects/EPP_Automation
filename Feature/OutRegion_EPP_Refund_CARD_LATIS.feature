Feature: validate login with Transaction Type "OutRegion_EPP_Refund_CARD_LATIS"

@OutRegion_EPP_Refund_CARD_LATIS @all
Scenario: Validating login functionality with LATIS and CARD
Given I am in EPP url with "OutRegion_EPP_Refund_CARD_LATIS"
When I login to the application
Then I click to the search button
Then I select search type
Then I verify PaymentID
Then I click on the payment_id
Then I click on the Refund_Btn
Then I verify refund_id
Then I logout