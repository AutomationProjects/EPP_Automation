package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.HomePage;
import com.EPP_Automation.BusinessSpecific.LoginLogoutPage;
import com.EPP_Automation.Common.CommonClass;
import com.EPP_Automation.Common.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginLogoutHomePageStepDefinition extends ExcelOperations {

	ExcelOperations opr =  new ExcelOperations();

	ExcelUtils excelUtils = new ExcelUtils();
	@Given("^I am in EPP url with \"([^\"]*)\"$")
	public void i_am_in_EPP_url(String data) throws Throwable {
		CommonClass.data(data);
		LoginLogoutPage.goToEPPLoginPage();
	}

	@When("^I login to the application$")
	public void i_login_to_the_application() throws Throwable {
		//opr.setVariablesFromExcelRCSheet();
		LoginLogoutPage.loginCredentials();
	}

	@Then("^I click on Out Region$")
	public void i_click_on_Out_Region() throws Throwable {
		HomePage.outRegionButtion();
	}


	@Then("^I enter accountNumber$")
	public void i_enter_accountNumber() throws Throwable {
		HomePage.accountInforPageSteps();
	}

	@Then("^I select Transaction type$")
	public void i_select_Transaction_type_and_press_accountlookup() throws Throwable {
		HomePage.transactionType();
	}

	@Then("^I enter Order Number$")
	public void i_enter_Order_Number() throws Throwable {
		//HomePage.orderNumber();
	}

	@Then("^I select Market Unit$")
	public void i_select_Market_Unit() throws Throwable {
		//HomePage.MarketUnit();
	}

	@Then("^I logout$")
	public void i_logout() throws Throwable {
		LoginLogoutPage.logout();
	}
	
	@Then("^I click on Bart$")
    public void i_click_on_Bart() throws Throwable {
          HomePage.bartButton();
    }

}
