package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.RecurringPaymentPage;

import cucumber.api.java.en.Then;

public class RecurringPaymentsPageStepDefinition {
	
	
	@Then("^I click on Automatic Payment Unenrollment button$")
	public void i_click_on_Automatic_Payment_Unenrollment_button() throws Throwable {
		RecurringPaymentPage.clickConfirmUnenrollment();
	}
	
	@Then("^I click on Unenrollment radio button$")
	public void i_click_on_Unenrollment_radio_button() throws Throwable {
		RecurringPaymentPage.clickUnenrollmentRadiobutton();
	}
	
}
