package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.TransactionInformationPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class TransactionInformationStepDefinition {
	
	ExcelOperations opr =  new ExcelOperations();
	
    @And("^I fill all mandatory fields$")
    
	public void i_fill_all_mandatory_fields() throws Throwable {
    //	opr.setVariablesFromExcelRCSheet();
	  TransactionInformationPage.fillFields();
	//  TransactionInformationPage.FillFieldsNewService();
	}
    //ab87891
    @And("^I fill all mandatory fields for new service deposit$")
    
   	public void i_fill_all_mandatory_fields_for_new_service_deposit() throws Throwable {
       //	opr.setVariablesFromExcelRCSheet();
   	  //TransactionInformationPage.fillFields();
   	  TransactionInformationPage.FillFieldsNewService();
   	}
	
    @And("^I fill all the mandatory fields$")
   
    public void i_fill_all_the_mandatory_fields() throws Throwable {
        TransactionInformationPage.Transaction_fillImpFields();
   	}
    
    @Then("^I validate whether account lookup is successful")
	public void i_validate_account_lookup_success() throws Throwable {
    	TransactionInformationPage.validateAccountLookupSuccess();
	}
}
