package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.NegotiatePaymentPage;
import com.EPP_Automation.BusinessSpecific.SearchPage;

import cucumber.api.java.en.Then;

public class SearchPageStepDefinition extends ExcelOperations {

	ExcelOperations opr =  new ExcelOperations();

	@Then("^I click to the search button$")
	public void i_click_to_the_search_button() throws Throwable {
		SearchPage.clickOnSearchButton();
	}

	@Then("^I select search type$")
	public void i_select_search_type() throws Throwable {
		SearchPage.selectSearchType();
	}
	
	@Then("^I select search type for BART$")
	public void i_select_search_type_for_BART() throws Throwable {
		SearchPage.selectSearchTypeforBART();
	}
	
	@Then("^I verify PaymentID$")
	public void i_verify_PaymentID() throws Throwable {
		SearchPage.DBVerification();
	}
	
	@Then("^I click on the payment_id$")
	public void i_click_on_payment_id$() throws Throwable {
		SearchPage.clickOnPayment_Id();
	}
	
	@Then("^I verify refund_id$")
	public void i_verify_refund_id$() throws Throwable {
		SearchPage.VerifyRefundID();
	}
	
	@Then("^Verify and search for last Payment ID$")
	public void verify_and_search_for_last_Payment_ID$() throws Throwable {
		NegotiatePaymentPage.VerifySearchLstPayID();
	}
	
	@Then("^I click on the Refund_Btn$")
	public void i_click_on_Refund_Btn$() throws Throwable {
		SearchPage.clickOnRefundButton();
	}
	
	@Then("^Validate searched result$")
	public void validate_searched_result$() throws Throwable {
		SearchPage.ValidateSearch();
	}
	
}
