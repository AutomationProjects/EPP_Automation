package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.PaymentOptionPage;
import cucumber.api.java.en.Then;

public class PaymentOptionPageStepDefination extends ExcelOperations {
	ExcelOperations opr =  new ExcelOperations();

	@Then("^I click to the Payment option button$")
	public void i_click_to_the_Payment_option_button() throws Throwable {
	PaymentOptionPage.clickOnPaymentOption();}

	@Then("^I enter Billing Application Account Id$")
	public void i_enter_Billing_Application_Account_Id() throws Throwable {
		System.out.println("before");
		PaymentOptionPage.billingapplicationaccount_id();
		PaymentOptionPage.selectbillingapplicationsystem();
		
		
		System.out.println("after");
	}

}
