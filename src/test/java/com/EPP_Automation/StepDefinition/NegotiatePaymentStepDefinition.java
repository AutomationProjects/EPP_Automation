package com.EPP_Automation.StepDefinition;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.NegotiatePaymentPage;

import cucumber.api.java.en.Then;

public class NegotiatePaymentStepDefinition  extends ExcelOperations {

	ExcelOperations opr =  new ExcelOperations();

	@Then("^fill NegotiatePayment fields$")
	public void fill_NegotiatePayment_fields() throws Throwable {
		//opr.setVariablesFromExcelIFrameSheet();
		NegotiatePaymentPage.NegotiatePaymentFields();
	}
	
	@Then("^I verify confirmation Number$")
	public void i_verify_confirmation_Number() throws Throwable {
		NegotiatePaymentPage.DBVerification();
	}

	@Then("^I verify confirmation Number for multipayment$")
	public void i_verify_confirmation_Number_for_multipayment() throws Throwable {
		NegotiatePaymentPage.DBVerificationForMultiPayment();
	}
}
