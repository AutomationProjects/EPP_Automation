package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentOptionPageFactory {
	
	@FindBy(xpath=".//*[@id='billingAccountId']")
	public static WebElement billingapplicationaccountid; 
	
	@FindBy(xpath=".//*[@id='billingAppSystem']")
	public static WebElement billingapplicationsystem; 
 
	@FindBy(id="next")
	public static WebElement searchbutton;  
	
	@FindBy(tagName="iframe")
	public static WebElement frame;
}


