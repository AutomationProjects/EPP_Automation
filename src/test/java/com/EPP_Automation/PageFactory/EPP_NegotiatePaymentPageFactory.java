package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EPP_NegotiatePaymentPageFactory {
	
	@FindBy(xpath="//iframe[@scrolling='yes']")
	public static WebElement switchToIframe;
	
	@FindBy(xpath="//label[text()='Method of Payment:']/following-sibling::select")
	public static WebElement methodOfPayment;
	
	@FindBy(xpath="//input[@name='cardNum']")
	public static WebElement cardNumber;
	
	@FindBy(id="expiryMonth")
	public static WebElement expiryMonth;
	
	@FindBy(id="expiryYear")
	public static WebElement expiryYear;
	
	@FindBy(xpath="//input[@name='cardZip']")
	public static WebElement zipCode;
	
	@FindBy(xpath="//span[@class='checkbox-disable-icon']")
	public static WebElement checkBox;
	
	@FindBy(id="ValidatePaymentInfo")
	public static WebElement nextButtton;
	
	@FindBy(id="Email")
	public static WebElement emailBox;
	
	@FindBy(id="submitPayment")
	public static WebElement submitPayment;
	
	@FindBy(name="routingNum")
	public static WebElement routingNumber;

	@FindBy(name="bankAccntNum")
	public static WebElement accountNumber;
	
	@FindBy(xpath="//table[@id='tabResult']//tr//td//font[@class='GRAYED']")
	public static WebElement confirmationNumber;

	@FindBy(xpath="(//table[@id='tabResult']//tbody//tr//td//font[@class='GRAYED'])[1]")
	public static WebElement confirmationNumberForMultiPayment;
	 
	

}
