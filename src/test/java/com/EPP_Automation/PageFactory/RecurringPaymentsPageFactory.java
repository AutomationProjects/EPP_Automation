package com.EPP_Automation.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class RecurringPaymentsPageFactory {
	
	//for Recurring Payment Options
	
	@FindBy(xpath=".//*[@id='autoBtn']")
	public static WebElement clickConfirmUnenrollmentButton;
	
	@FindBy(xpath="//td[@class='CONTENT']")
	public static WebElement PaymentMethod ;
	
	@FindBy(xpath="//input[@value='auto']")
	public static WebElement AutomaticPaymentEnrollment;
	
	@FindBy(xpath="//input[@value='noAuto']")
	public static WebElement AutomaticPaymentUnEnrollment;
	
	@FindBy(name="updBtn")
	public static WebElement UpdateBillingInfo; 

	@FindBy(id="payFrame")
	public static WebElement NegotiateChangeFrame;
	
	@FindBy(id="PageContent_ADD_CC_LINK")
	public static WebElement AddNewDebitCreditCard;
	
	@FindBy(name="NICKNAME")
	public static WebElement AccountNickName;
	
	@FindBy(name="DEBIT_ACCOUNT")  
	public static WebElement CardNumber;

	@FindBy(name="Next")
	public static WebElement NextButton;
	
	@FindBy(name="CARD_EXPIRATION_MONTH")
	public static WebElement CardExpMonth;
	
	@FindBy(name="CARD_EXPIRATION_YEAR")
	public static WebElement CardExpYear;
	
	@FindBy(name="DEBIT_ZIP")
	public static WebElement CardBillingZip; 
	
	@FindBy(name="Submit9")
	public static WebElement FinishButton;
	
	
	
}
