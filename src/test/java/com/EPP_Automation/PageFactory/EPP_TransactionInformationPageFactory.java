package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EPP_TransactionInformationPageFactory {
	
	@FindBy(xpath="(//td[contains(text(),'Add New')])[1]")
	public static WebElement addNewPaymentText;
	
	@FindBy(id="id_PayerName")
	public static WebElement enterFirstName;
	
	@FindBy(xpath=".//*[@class='CONTENT'][text()='First Name:']")
	public static WebElement FirstNameLabel;
	
	@FindBy(xpath=".//*[@class='CONTENT'][text()='Business Name:']")
	public static WebElement BizNameLabel;

	@FindBy(id="id_PayerLastName")
	public static WebElement enterLastName;

	@FindBy(name="custConvChk")
	public static WebElement checkConvenienceFeeConfirmation;

	@FindBy(name="addBtn")
	public static WebElement addPayment;

	@FindBy(id="id_Pmt")
	public static WebElement enterPaymentAmount;

	@FindBy(xpath="//input[@id='id_PayDate']")
	public static WebElement enterPaymentDate;

	@FindBy(id="id_Addr1")
	public static WebElement address;

	@FindBy(id="id_City")
	public static WebElement city;

	@FindBy(id="id_State")
	public static WebElement state;

	@FindBy(id="id_Zip")
	public static WebElement zipcode;

	@FindBy(id="id_EntityAmount0")
	public static WebElement enterEntityInfo;

	@FindBy(id="id_EntityName0")//need to enter xpath //*[@id='id_EntityName0']
	public static WebElement selectEntityCode;
	
	@FindBy(xpath="//td[contains(text(),'Add New Payment')]")
	public static WebElement AddNEWPaymentPage;
//entitycodetitle://td[@class='COLUMNHEADER'][2]
	//for MultiPayment

	@FindBy(xpath="//table[@id='tbl']//tr//td//input[@id='id_PayerBizName']")
	public static WebElement enterBussinessName;

	@FindBy(xpath="//input[@id='id_AddBtn']")
	public static WebElement enterAditionalBAN;
	
	@FindBy(id="id_BtnAddBTN")
	public static WebElement addAditionalBAN;
	
	@FindBy(id="amtTf1")
	public static WebElement aditionalAmount;//For scheduled payment
	@FindBy(xpath="//img[contains(@title,'Calendar')]")
	public static WebElement calendarButton;
	
    //For BART payment
	@FindBy(xpath="//*[@id='id_PayerBizName']")
	public static WebElement bartbusinessname;
	
	@FindBy(xpath="//input[@name='addbtnTf']")
	public static WebElement AdditionalInvc;
	
	@FindBy(xpath=".//*[@id='amtTf3']")
	public static WebElement ThirdAdditionalAmt;
	
	@FindBy(xpath=".//*[@id='amtTf9']")
	public static WebElement lastAdditionalAmt;
	
	@FindBy(css="a>font[color='red']")
	public static WebElement dates;
}
