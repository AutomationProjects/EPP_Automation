package com.EPP_Automation.PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EPP_HomePageFactory {
	
	@FindBy(id="id_Account")
	public static WebElement enterAccountNumber;

	@FindBy(id="id_SaleType")
	public static WebElement selectTransactionType;
	
	@FindBy(name="addBtn")
	public static WebElement clickOnAccountLookup;
	
	@FindBy(id="id_Order")
	public static WebElement enterOrderNumber;
	
	@FindBy(id="id_Suffix")
	public static WebElement enterAccountSuffix;
	
	@FindBy(id="id_MktUnit")
	public static WebElement selectMarketUnit;

	@FindBy(xpath="(//input[@name='regionBtn'])[2]")
	public static WebElement clickOnOutRegionRadioButton;
	
	@FindBy(xpath="(//img[@title='Search Option'])[1]")
	public static WebElement clickOnSearchButton;
	
	@FindBy(id="IPS_Top_Anc_ID")
	public static WebElement clickonpaymentoption;
	
	@FindBy(id="BartChk_id")
    public static WebElement clickOnBartButton;

}

