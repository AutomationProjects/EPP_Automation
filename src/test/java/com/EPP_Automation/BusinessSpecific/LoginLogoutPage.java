package com.EPP_Automation.BusinessSpecific;

import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.PageFactory.EPP_LoginLogoutPageFactory;
import com.EPP_Automation.PageFactory.EPP_SearchPageFactory;
import com.sun.jna.platform.win32.WinDef.CHAR;


public class LoginLogoutPage extends CommonSeleniumClass{

	public static void goToEPPLoginPage(){

		CommonSeleniumClass.initiatePageFactory();
		/*String Environment = GlobalVariables.Ex_EPPURL;
		String[] arrSplit = Environment.split("https://eppjws-"[a-z][1-3]+".test.intranet/epp/eppSearch.jsp");
		String env = Environment.substring(15, 20);	
		System.out.println("Current testing environment is: " +env);*/
		selectRequiredBrowser(GlobalVariables.Ex_BrowserToUse);
		navigate(GlobalVariables.Ex_EPPURL);
		maximizeWindow();

	}
	
	public static void getEnvironment(){
		String Environment = GlobalVariables.RC_EPPURL;
		String env = Environment.substring(14, 20);
		System.out.println("Current environment is " +env);
				
				}
	public static void loginCredentials(){
		CommonSeleniumClass.initiatePageFactory();
		enter(EPP_LoginLogoutPageFactory.userName,GlobalVariables.Ex_Username);
		enter(EPP_LoginLogoutPageFactory.password,GlobalVariables.Ex_Password);
		click(EPP_LoginLogoutPageFactory.clickOnLogIn);
		
	}


	public static void logout(){
		CommonSeleniumClass.initiatePageFactory();
		think(1);
		//click(EPP_LoginLogoutPageFactory.clickOnLogOut);
		closeAllWindows();
	}

}