package com.EPP_Automation.BusinessSpecific;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import com.EPP_Automation.Common.CommonClass;
import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.CalendarPageFactory;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;
import com.EPP_Automation.PageFactory.EPP_TransactionInformationPageFactory;
import com.EPP_Automation.PageFactory.RecurringPaymentsPageFactory;

import cucumber.api.java.en.Then;

public class TransactionInformationPage extends CommonSeleniumClass {
	
	
	
	private static final WebDriver WebDriver = null;

	public static void fillFields() throws Throwable{

		/*String actualCityData = getAttribute(EPP_TransactionInformationPageFactory.city, "value");

		String actualBussinessName = getAttribute(EPP_TransactionInformationPageFactory.enterBussinessName, "value");

	try{
			if(actualCityData.equalsIgnoreCase(""))
			{	
				enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
				enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);
				enter(EPP_TransactionInformationPageFactory.address,GlobalVariables.Ex_Address);
				enter(EPP_TransactionInformationPageFactory.city,GlobalVariables.Ex_City);
				enter(EPP_TransactionInformationPageFactory.state,GlobalVariables.Ex_State);
				enter(EPP_TransactionInformationPageFactory.zipcode,GlobalVariables.Ex_ZipCode);
			click(EPP_TransactionInformationPageFactory.calendarButton);
				String Query = "select sysdate from dual" ;
				
				clear(EPP_TransactionInformationPageFactory.enterPaymentDate);
				enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
				//clear(EPP_TransactionInformationPage.enterPaymentAmount);
				//enter(EPP_TransactionInformationPage.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
				selectOptionByVisibleText(EPP_TransactionInformationPageFactory.selectEntityCode,GlobalVariables.Ex_EntityCode);
				clear(EPP_TransactionInformationPageFactory.enterEntityInfo);
				enter(EPP_TransactionInformationPageFactory.enterEntityInfo,GlobalVariables.Ex_EntityInfoAmount);
				//click(EPP_TransactionInformationPage.checkConvenienceFeeConfirmation);
			click(EPP_TransactionInformationPageFactory.addPayment);
		}*/
		
		switch(GlobalVariables.Ex_PaymentType){
		
		case "MultiPayment":
			enter(EPP_TransactionInformationPageFactory.enterBussinessName,GlobalVariables.Ex_BusinessName);
			clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
			enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
			  String AdditonalBAN=(GlobalVariables.Ex_AdditionalBAN);
	            System.out.println(AdditonalBAN);
	                String[] arr1Split = AdditonalBAN.split(",");
	                System.out.println("length of additional ban is:" + arr1Split.length );
	                for (int i=0; i< arr1Split.length; i++)
	                {
	                        click(EPP_TransactionInformationPageFactory.enterAditionalBAN);
	                        enter(EPP_TransactionInformationPageFactory.enterAditionalBAN,arr1Split[i]);
	                        click(EPP_TransactionInformationPageFactory.addAditionalBAN);
	                   //    try{
	                      //  CommonSeleniumClass.dismissAlert(null);
	                  //      }catch(Exception e){
	                   //   clear(EPP_TransactionInformationPageFactory.AdditionalAmt);
	                   //  System.out.println("Additional Amount to be paid:" +CommonClass.getAmountToBePaid());
	                   //   enter(EPP_TransactionInformationPageFactory.AdditionalAmt,CommonClass.getAmountToBePaid());

	                //}
	          }
	            //    clear(EPP_TransactionInformationPageFactory.ThirdAdditionalAmt); 
	            //    enter(EPP_TransactionInformationPageFactory.ThirdAdditionalAmt,GlobalVariables.Ex_PaymentAmount);
	            //    clear(EPP_TransactionInformationPageFactory.lastAdditionalAmt); 
	             //   enter(EPP_TransactionInformationPageFactory.lastAdditionalAmt,GlobalVariables.Ex_PaymentAmount);
	                click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
	                click(EPP_TransactionInformationPageFactory.addPayment); 
	            
		//	enter(EPP_TransactionInformationPageFactory.enterAditionalBAN,GlobalVariables.Ex_AdditionalBAN);
			
			
			//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
		//	clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
		//	enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
		//	click(EPP_TransactionInformationPageFactory.addAditionalBAN);
		//	clear(EPP_TransactionInformationPageFactory.aditionalAmount);
		//	enter(EPP_TransactionInformationPageFactory.aditionalAmount,GlobalVariables.Ex_AditionalAmount);
		//	click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
		//	click(EPP_TransactionInformationPageFactory.addPayment);
		//	click(EPP_TransactionInformationPageFactory.calendarButton);
		//	click(EPP_TransactionInformationPageFactory.dates);
			//CommonSeleniumClass.acceptAlert(WebDriver);
			//enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
			break;
		
		case "SinglePayment":			
			try{
				if(EPP_TransactionInformationPageFactory.BizNameLabel.isDisplayed()){
					enter(EPP_TransactionInformationPageFactory.enterBussinessName,GlobalVariables.Ex_BusinessName);
				}
				   }catch(Exception e){
			        //   EPP_TransactionInformationPageFactory.FirstNameLabel.isDisplayed()
					enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
					enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);		
			        }			     		
			
			try{
				if((GlobalVariables.Ex_ScheduleType.equals("SinglePayment"))){
				click(EPP_TransactionInformationPageFactory.calendarButton);
				click(EPP_TransactionInformationPageFactory.dates);
				}
				}
				catch(Exception e){
				enterScheduledPaymentDate();					
				}
			clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
			enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
			click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
			click(EPP_TransactionInformationPageFactory.addPayment);
			
			
			break;
				
		case "BartSinglePayment":
			try{
				if(EPP_TransactionInformationPageFactory.FirstNameLabel.isDisplayed()){
				enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
				enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);		
				}
			}catch(Exception e){
					   //(EPP_TransactionInformationPageFactory.BizNameLabel.isDisplayed()){
				enter(EPP_TransactionInformationPageFactory.enterBussinessName,GlobalVariables.Ex_BusinessName);	
	         }try{
	        	 if((GlobalVariables.Ex_ScheduleType.equals("SinglePayment"))){
				click(EPP_TransactionInformationPageFactory.calendarButton);
				click(EPP_TransactionInformationPageFactory.dates);
				}
	         }catch(Exception e){
				enterScheduledPaymentDate();					
				}
        //    String BartZip = EPP_TransactionInformationPageFactory.zipcode.getAttribute("value");
     //      try{if(BartZip.isEmpty())
        //    {enter(EPP_TransactionInformationPageFactory.zipcode,GlobalVariables.Ex_Zipcode);          
         //   }
     //      }catch(Exception e)
        //   {
	        clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
        	enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
			click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
        	click(EPP_TransactionInformationPageFactory.addPayment);
               
      break;
      
      case "BartMultiInvoicePayment":
            enter(EPP_TransactionInformationPageFactory.bartbusinessname,GlobalVariables.Ex_BusinessName);
            clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
            enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
            click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
            String BartZip1=EPP_TransactionInformationPageFactory.zipcode.getAttribute("value");
            if (BartZip1.isEmpty())
            {
                  enter(EPP_TransactionInformationPageFactory.zipcode,GlobalVariables.Ex_Zipcode);          
            }

            String BartInv=(GlobalVariables.Ex_AdditionalBAN);
            System.out.println(BartInv);
                String[] arrSplit = BartInv.split(",");
                for (int i=0; i< arrSplit.length; i++)
                {
                        click(EPP_TransactionInformationPageFactory.AdditionalInvc);
                        enter(EPP_TransactionInformationPageFactory.AdditionalInvc,arrSplit[i]);
                        click(EPP_TransactionInformationPageFactory.addAditionalBAN);
                      //  enter(EPP_TransactionInformationPageFactory.AdditionalAmt,CommonClass.getAmountToBePaid());

                }
            click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
            click(EPP_TransactionInformationPageFactory.addPayment); 
            
		default:
			System.out.println();
		}
			
	}
	
	public static void ValidateMaxPaymentAMT(){
		
	}
	
	
	public static void validateAccountLookupSuccess(){
		if(EPP_TransactionInformationPageFactory.addNewPaymentText.isDisplayed()){
			System.out.println("AccountLookupSuccessful");
		}
	}
	public static void Transaction_fillImpFields()
	{
		if(getText(RecurringPaymentsPageFactory.PaymentMethod).equalsIgnoreCase("not enrolled in autopay"))
				{
			       click(RecurringPaymentsPageFactory.AutomaticPaymentEnrollment);
			       click(RecurringPaymentsPageFactory.UpdateBillingInfo);
			       switchToFrameByElement(RecurringPaymentsPageFactory.NegotiateChangeFrame);
			       click(RecurringPaymentsPageFactory.AddNewDebitCreditCard);
			       enter(RecurringPaymentsPageFactory.AccountNickName, GlobalVariables.Ex_CustomerFirstName);
			       enter(RecurringPaymentsPageFactory.CardNumber, GlobalVariables.Ex_CardNumber);
			       selectOptionByVisibleText(RecurringPaymentsPageFactory.CardExpMonth,GlobalVariables.Ex_ExpiryMonth);//element are not inspecting
				   selectOptionByVisibleText(RecurringPaymentsPageFactory.CardExpYear,GlobalVariables.Ex_ExpiryYear);//element are not inspecting
			       enter(RecurringPaymentsPageFactory.CardBillingZip,GlobalVariables.Ex_Zipcode);
			       click(RecurringPaymentsPageFactory.FinishButton);
				}
		else
		{
			click(RecurringPaymentsPageFactory.AutomaticPaymentUnEnrollment);
		}
	}
	

	//ab87891
	public static void FillFieldsNewService() throws ClassNotFoundException, SQLException{ 
		if(EPP_TransactionInformationPageFactory.AddNEWPaymentPage.isDisplayed()){
			String pagetitle=EPP_TransactionInformationPageFactory.AddNEWPaymentPage.getText();
			System.out.println("The title of page is"+ pagetitle);
		enter(EPP_TransactionInformationPageFactory.enterFirstName,GlobalVariables.Ex_CustomerFirstName);
		enter(EPP_TransactionInformationPageFactory.enterLastName,GlobalVariables.Ex_CustomerLastName);
        enter(EPP_TransactionInformationPageFactory.address,GlobalVariables.Ex_Address);
		enter(EPP_TransactionInformationPageFactory.city,GlobalVariables.Ex_City);
		enter(EPP_TransactionInformationPageFactory.state,GlobalVariables.Ex_State);
		enter(EPP_TransactionInformationPageFactory.zipcode,GlobalVariables.Ex_ZipCode);
		clear(EPP_TransactionInformationPageFactory.enterPaymentDate);
		enter(EPP_TransactionInformationPageFactory.enterPaymentDate,GlobalVariables.Ex_PaymentDate);
		}
		clear(EPP_TransactionInformationPageFactory.enterPaymentAmount);
		enter(EPP_TransactionInformationPageFactory.enterPaymentAmount,GlobalVariables.Ex_PaymentAmount);
		click(EPP_TransactionInformationPageFactory.checkConvenienceFeeConfirmation);
		//click(EPP_TransactionInformationPageFactory.addPayment);
//selectOptionByVisibleText(EPP_TransactionInformationPageFactory.selectEntityCode,GlobalVariables.Ex_EntityCode);
			//clear(EPP_TransactionInformationPageFactory.enterEntityInfo);
			//enter(EPP_TransactionInformationPageFactory.enterEntityInfo,GlobalVariables.Ex_EntityInfoAmount);

			click(EPP_TransactionInformationPageFactory.addPayment);
		//alert();	
			}
	


  public static void enterScheduledPaymentDate() throws Throwable, SQLException{
	  if(GlobalVariables.Ex_ScheduleType.equals("Scheduled")){
			click(EPP_TransactionInformationPageFactory.calendarButton);
			String Query = "select sysdate from dual" ;
			ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","EPWF_APP", "epwf_app_epwfst1", Query);
			String date = null;
			while (rset.next()) {
				date =rset.getString("SYSDATE").substring(8,10);
			}
			int dateInt=Integer.parseInt(date);
			String parentWindow = driver.getWindowHandle();
			Set<String> handles =  driver.getWindowHandles();
			for(String windowHandle  : handles)
		       {
		       if(!windowHandle.equals(parentWindow))
		          {
		    	   	driver.switchTo().window(windowHandle);
		    	   	try{
		    	   		click(CalendarPageFactory.getDate(dateInt+1, driver));
		    	   	}catch(Exception e){
		    	   		
		    	   	}
		    	   	driver.switchTo().window(parentWindow);
		          }
		       }
  }
  

}
  
  
	}
  
  

