package com.EPP_Automation.BusinessSpecific;

import java.io.File;
import java.util.ArrayList;

public class GlobalVariables {

	//Excel generic file path
	public static String location = new File("src/test/java/com/./EPP_Automation/./TestData").getAbsolutePath();
	public static File ExcelFilePath = new File(location + "\\TestData.xlsx");

	//Excel data into java variables RC SHEET
	public static String Ex_Username;
	public static String Ex_Password;
    public static String Ex_BusinessName;
	public static String Ex_CustomerFirstName;
	public static String Ex_CustomerLastName;
	public static String Ex_BTN;
	public static String Ex_EPPURL;
	public static String Ex_BrowserToUse;
	public static String Ex_TransactionType;
	public static String Ex_PaymentAmount;
	public static String Ex_ScheduleType;
	public static String Ex_PaymentType;
	public static String Ex_OrderNumber;
	public static String Ex_AccountSuffix;
	public static String Ex_MarketUnit;
	public static String Ex_PaymentDate;
	public static String Ex_Address;
	public static String Ex_State;
	public static String Ex_City;
	public static String Ex_ZipCode;
	public static String Ex_EntityInfoAmount;
	public static String Ex_AdditionalBAN;
	public static String Ex_EntityCode;
	public static String Ex_AditionalAmount;
	public static String Ex_SearchType;
	public static String Ex_SearchValue;
	public static String Ex_ToDate;
	public static String Ex_FromDate;
	public static String Ex_Billingapplicationsystem;
	public static String Ex_Billingapplicationaccountid;
	public static String Ex_ConfirmationNo;
	
	//Excel column names RequiredCredentials sheet
	public final static String RC_SheetName = "RequiredCredentials";
	public final String RC_Username = "Username";
	public final String RC_Password = "Password";
	public final String RC_BusinessName = "BusinessName";
	public final String RC_CustomerFirstName = "CustomerFirstName";
	public final String RC_CustomerLastName = "CustomerLastName";
	public final String RC_BTN = "BTN";
	public final static String RC_EPPURL = "EPPURL";
	public final static String DB_Username= "epwf_app";
	public final static String DB_Client = "jdbc:oracle:thin";
	public final static String DB_dns_ITV1 = "epwfst1db12c.test.intranet";
	public final static String DB_SID_ITV1 = "epwfst1";
	public final static String DB_port_ITV1 = "1602";
	public final static String DB_PASS_ITV1 = "epwf_app_epwfst1";
	public final static String EPPURL_ITV1 = "https://eppjws-test1.test.intranet/epp/";
	public final static String EPPURL_E2E = "https://eppjws-e2e.test.intranet/epp/";
	public final String RC_BrowserToUse = "BrowserToUse";
	public static String RC_TransactionType = "TransactionType";
	public static String RC_PaymentAmount = "PaymentAmount";
	public static String RC_OrderNumber = "OrderNumber";
	public static String RC_AccountSuffix = "AccountSuffix";
	public static String RC_MarketUnit = "MarketUnit";
	public static String RC_PaymentDate = "PaymentDate";
	public static String RC_Address = "Address";
	public static String RC_State = "State";
	public static String RC_City = "City";
	public static String RC_ZipCode = "ZipCode";
	public static String RC_EntityInfoAmount = "EntityInfoAmount";
	public static String RC_AdditionalBAN = "AdditionalBAN";
	public static String RC_EntityCode = "EntityCode";
	public static String RC_AditionalAmount = "AditionalAmount";
	public static String executeStr = "Y";	
	public static String RC_SearchType = "SearchType";
	public static String RC_SearchValue = "SearchValue";
	public static String RC_ToDate = "ToDate";
	public static String RC_FromDate = "FromDate";
	public static String RC_ScheduleType="ScheduleType";
	public static String RC_Billingapplicationsystem="Billingapplicationsystem";
	public static String RC_Billingapplicationaccountid="Billingapplicationaccountid";

	public static String RC_PaymentType="PaymentType";
	//Excel data into java variables IFrame SHEET
	public static String Ex_MethodOfPayment;
	public static String Ex_CardNumber;
	public static String Ex_ExpiryMonth;
	public static String Ex_ExpiryYear;
	public static String Ex_Zipcode;
	public static String Ex_EmailId;
	public static String Ex_RoutingNumber;
	public static String Ex_AccountNumber;
	public static ArrayList<Integer> rowNumList;
	public static int currRowNumber;
	public static String RC_ConfirmationNo="ConfirmationNo";
	
	//Excel column names IFrame sheet
	//public final static String IF_SheetName = "IFrame";
	public final String IF_MethodOfPayment = "MethodOfPayment";
	public final String IF_CardNumber = "CardNo.";
	public final String IF_ExpiryMonth = "ExpiryMonth";
	public final String IF_ExpiryYear = "ExpiryYear";
	public final String IF_Zipcode = "ZipCode";
	public final String IF_EmailId = "Email id";
	public final String IF_RoutingNumber = "Routing no.";
	public final String IF_AccountNumber = "Account no.";
	

}
