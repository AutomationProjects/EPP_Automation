package com.EPP_Automation.BusinessSpecific;

import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.PageFactory.EPP_HomePageFactory;
import com.thoughtworks.selenium.webdriven.commands.Click;

public class HomePage extends CommonSeleniumClass {	

	public static void outRegionButtion()
	{
		click(EPP_HomePageFactory.clickOnOutRegionRadioButton);
	}
	
	public static void bartButton(){
        click(EPP_HomePageFactory.clickOnBartButton);
  }

	public static void accountInforPageSteps()
	{		

		enter(EPP_HomePageFactory.enterAccountNumber,GlobalVariables.Ex_BTN);
		
	}	

	/*public static void transactionType()
	{
		selectOptionByVisibleText(EPP_HomePageFactory.selectTransactionType,GlobalVariables.Ex_TransactionType);

		try{
			if(getAttribute(EPP_HomePageFactory.enterOrderNumber,"value").equalsIgnoreCase(""))
			{
				enter(EPP_HomePageFactory.enterOrderNumber,GlobalVariables.Ex_OrderNumber);
				selectOptionByVisibleText(EPP_HomePageFactory.selectMarketUnit,GlobalVariables.Ex_MarketUnit);
				click(EPP_HomePageFactory.clickOnAccountLookup);
			}
			else
			{
				enter(EPP_HomePageFactory.enterAccountSuffix,GlobalVariables.Ex_AccountSuffix);
				selectOptionByVisibleText(EPP_HomePageFactory.selectMarketUnit,GlobalVariables.Ex_MarketUnit);
				click(EPP_HomePageFactory.clickOnAccountLookup);

			}
		}catch(Exception e)
		{

			click(EPP_HomePageFactory.clickOnAccountLookup);

		}
	}*/

	public static void transactionType()
	{
		selectOptionByVisibleText(EPP_HomePageFactory.selectTransactionType,GlobalVariables.Ex_TransactionType);
		switch (GlobalVariables.Ex_TransactionType) {

		case "New Service Deposit":

			enter(EPP_HomePageFactory.enterOrderNumber,GlobalVariables.Ex_OrderNumber);
			selectOptionByVisibleText(EPP_HomePageFactory.selectMarketUnit,GlobalVariables.Ex_MarketUnit);
			click(EPP_HomePageFactory.clickOnAccountLookup);

			break;

		case "Advance Payment":

			enter(EPP_HomePageFactory.enterOrderNumber,GlobalVariables.Ex_OrderNumber);
			selectOptionByVisibleText(EPP_HomePageFactory.selectMarketUnit,GlobalVariables.Ex_MarketUnit);
			click(EPP_HomePageFactory.clickOnAccountLookup);

			break;

		case "Entity Final Payment":

			enter(EPP_HomePageFactory.enterAccountSuffix,GlobalVariables.Ex_AccountSuffix);
			selectOptionByVisibleText(EPP_HomePageFactory.selectMarketUnit,GlobalVariables.Ex_MarketUnit);
			click(EPP_HomePageFactory.clickOnAccountLookup);

			break;
			
		case "Recurring Payment Options":

			click(EPP_HomePageFactory.clickOnAccountLookup);

			break;


		default :

			click(EPP_HomePageFactory.clickOnAccountLookup);
		}
		
	}

}