package com.EPP_Automation.BusinessSpecific;
import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.RecurringPaymentsPageFactory;

   public class RecurringPaymentPage extends CommonSeleniumClass {
 
	public static void clickConfirmUnenrollment()
	{
		click(RecurringPaymentsPageFactory.clickConfirmUnenrollmentButton);
	}
	
	public static void clickUnenrollmentRadiobutton()
	{
		click(RecurringPaymentsPageFactory.AutomaticPaymentUnEnrollment);
	}
	
	public static void fillImpFields()
	{
		if(getText(RecurringPaymentsPageFactory.PaymentMethod).equalsIgnoreCase("not enrolled in autopay"))
				{
			       click(RecurringPaymentsPageFactory.AutomaticPaymentEnrollment);
			       click(RecurringPaymentsPageFactory.UpdateBillingInfo);
			       switchToFrameByElement(RecurringPaymentsPageFactory.NegotiateChangeFrame);
			       click(RecurringPaymentsPageFactory.AddNewDebitCreditCard);
			       enter(RecurringPaymentsPageFactory.AccountNickName, GlobalVariables.Ex_CustomerFirstName);
			       enter(RecurringPaymentsPageFactory.CardNumber, GlobalVariables.Ex_CardNumber);
			       selectOptionByVisibleText(RecurringPaymentsPageFactory.CardExpMonth,GlobalVariables.Ex_ExpiryMonth);//element are not inspecting
				   selectOptionByVisibleText(RecurringPaymentsPageFactory.CardExpYear,GlobalVariables.Ex_ExpiryYear);//element are not inspecting
			       enter(RecurringPaymentsPageFactory.CardBillingZip,GlobalVariables.Ex_Zipcode);
			       click(RecurringPaymentsPageFactory.FinishButton);
				}
		else
		{
			click(RecurringPaymentsPageFactory.AutomaticPaymentUnEnrollment);
		}
	}
	
}
