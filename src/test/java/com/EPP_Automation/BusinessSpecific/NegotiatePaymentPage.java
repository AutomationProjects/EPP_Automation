package com.EPP_Automation.BusinessSpecific;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.EPP_Automation.Common.CommonClass;
import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.EPP_HomePageFactory;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;
import com.EPP_Automation.PageFactory.EPP_SearchPageFactory;
import com.EPP_Automation.PageFactory.EPP_TransactionInformationPageFactory;

public class NegotiatePaymentPage extends CommonSeleniumClass
{

	public static void NegotiatePaymentFields()
	{  
		switchToFrameByElement(EPP_NegotiatePaymentPageFactory.switchToIframe);

		selectOptionByVisibleText(EPP_NegotiatePaymentPageFactory.methodOfPayment,GlobalVariables.Ex_MethodOfPayment);

		switch (GlobalVariables.Ex_MethodOfPayment) {

		case "Credit or Debit Account":

			enter(EPP_NegotiatePaymentPageFactory.cardNumber,GlobalVariables.Ex_CardNumber);
			selectOptionByVisibleText(EPP_NegotiatePaymentPageFactory.expiryMonth,GlobalVariables.Ex_ExpiryMonth);
			selectOptionByVisibleText(EPP_NegotiatePaymentPageFactory.expiryYear,GlobalVariables.Ex_ExpiryYear);
			clear(EPP_NegotiatePaymentPageFactory.zipCode);
			enter(EPP_NegotiatePaymentPageFactory.zipCode,GlobalVariables.Ex_Zipcode);
			click(EPP_NegotiatePaymentPageFactory.checkBox);
			click(EPP_NegotiatePaymentPageFactory.nextButtton);
			think(2);
			click(EPP_NegotiatePaymentPageFactory.submitPayment);
			break;

		default :

			enter(EPP_NegotiatePaymentPageFactory.routingNumber,GlobalVariables.Ex_RoutingNumber);
			enter(EPP_NegotiatePaymentPageFactory.accountNumber,GlobalVariables.Ex_AccountNumber);
			click(EPP_NegotiatePaymentPageFactory.checkBox);
			click(EPP_NegotiatePaymentPageFactory.nextButtton);
			think(2);
			click(EPP_NegotiatePaymentPageFactory.submitPayment);
			
			break;
			
		case "BartSinglePayment":
		  enter(EPP_NegotiatePaymentPageFactory.emailBox,GlobalVariables.Ex_EmailId);
		  click(EPP_NegotiatePaymentPageFactory.submitPayment);
		  break;
		
		  case "BartMultiInvoicePayment":
		  click(EPP_NegotiatePaymentPageFactory.submitPayment);
		  break;

		}
	}
	
    //DB validation for single Payment
	public static void DBVerification() throws Exception{
		String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumber.getText();
		System.out.println("confirmationNumber ="+" "+confirmationNumber);
		String result= null;
		String Query = "Select PAYMENT_STATUS_CD from payment where payment_id in"+"('"+confirmationNumber+"')";
		//System.out.println("Query ="+Query);
		//DatabaseConn.run(Query);
		ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query);
		//System.out.println("rset = "+rset);
		while (rset.next()) {
			result =rset.getString("PAYMENT_STATUS_CD");
		}
		/*if(result.equals("Settlement_Completed")) 
		{
			System.out.println("verification successful");
		}

		else if(result.equals("Capture_Ready"))
		{
			System.out.println("verification successful");
		}
		else{
			System.out.println("verification successful");
		} */
		System.out.println(result);
	}
	
	// Validate and Search for Payment ID
	public static void VerifySearchLstPayID() throws SQLException, Throwable{		
		String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumber.getText();
		String result;
		System.out.println("confirmationNumber ="+" "+confirmationNumber);
		String Query = "Select PAYMENT_STATUS_CD from payment where payment_id in"+"('"+confirmationNumber+"')";
		//System.out.println("Query ="+Query);
		//DatabaseConn.run(Query);
	//	ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query);
		//System.out.println("rset = "+rset);
	//	while (rset.next()) {
	//		result =rset.getString("PAYMENT_STATUS_CD");
	//	}
		//switchToFrameByElement(EPP_SearchPageFactory.Searchbutton);
		click(EPP_SearchPageFactory.Searchbutton);
	//	By css = By.cssSelector("a[href='https://eppjws-test1.test.intranet/epp/eppSearch.jsp']");
	//	WebElement element = driver.findElement(css);
	//	((JavascriptExecutor)driver).executeScript("arguments[1].click();" , element);
		
		//((JavascriptExecutor)driver).executeScript("document.querySe‌​lector(\"a[href='https://eppjws-test1.test.intranet/epp/eppSearch.jsp'/]\"‌​).click();");
		
		enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchType);
		enter(EPP_SearchPageFactory.EnterSearchValue,confirmationNumber);
		clear(EPP_SearchPageFactory.FromDate);
		enter(EPP_SearchPageFactory.FromDate,GlobalVariables.Ex_FromDate);
		clear(EPP_SearchPageFactory.ToDate);
		enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
		click(EPP_SearchPageFactory.clickOnSearchNowButton);
   }
	    
	 
	//DB validation for Multi-payment Payment
	public static void DBVerificationForMultiPayment() throws Exception{
		String result=null;
		String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumberForMultiPayment.getText();
		System.out.println("PaymentVerificationReferenceValue="+confirmationNumber);
		String Query1 = "Select payment_id from payment where pmt_verification_reference_Val in "+"('"+confirmationNumber+"')";
		ResultSet rset1 = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query1);
		
		List<String> paymentId= new ArrayList<String>();
		List<String> paymentStatusCd= new ArrayList<String>();
		
		while (rset1.next()) {
			paymentId.add(rset1.getString("PAYMENT_ID"));
		}
		
		for(int i=0;i<paymentId.size();i++){
			String Query2 = "Select payment_status_cd from payment where payment_id in "+"('"+paymentId.get(i)+"')";
			ResultSet rset2 = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@151.117.87.205:1602:epwfst1","EPWF_APP", "epwf_app_epwfst1", Query2);
			while (rset2.next()) {
				paymentStatusCd.add(rset2.getString("PAYMENT_STATUS_CD"));
			}
		}
		
		for(int j=0;j<paymentId.size();j++){
			for(int k=j;k<=j;k++){
				if(k>0){
					result+=paymentId.get(j)+"("+paymentStatusCd.get(k)+")"+" ";
				}else{
					result=paymentId.get(j)+"("+paymentStatusCd.get(k)+")"+" ";
				}
			}	
		}
		System.out.println(result);
		//exo.setReportForTransaction(result,GlobalVariables.Re_SheetName,GlobalVariables.Re_PaymentResult);
	}

}







