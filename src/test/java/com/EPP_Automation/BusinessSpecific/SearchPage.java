package com.EPP_Automation.BusinessSpecific;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Assert;

import com.EPP_Automation.Common.CommonSeleniumClass;
import com.EPP_Automation.Common.DatabaseConn;
import com.EPP_Automation.PageFactory.EPP_HomePageFactory;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;
import com.EPP_Automation.PageFactory.EPP_SearchPageFactory;

public class SearchPage extends CommonSeleniumClass{

	public static void clickOnSearchButton()
	{
		click(EPP_HomePageFactory.clickOnSearchButton);
	}
	
	public static void clickOnPayment_Id(){
		click(EPP_SearchPageFactory.PaymentID);
	}
	
	public static void clickOnRefundButton()
    {
          click(EPP_SearchPageFactory.RefundButton);
    }

	
	public static void selectSearchType() throws SQLException, ClassNotFoundException
	{
		selectOptionByVisibleText(EPP_SearchPageFactory.SelectSearchType,GlobalVariables.Ex_SearchType);
		switch (GlobalVariables.Ex_SearchType) {

		case "Customer Last Name":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
		   
		    
			break;

		case "Order Number":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		case "Confirmation Number":
			String latest_PaymentID = null;
			String Query = "SELECT RNO,Payment_ID,Payment_status_cd FROM(SELECT ROW_NUMBER() OVER (ORDER BY created_Dttm desc) AS RNO,Payment_ID,Payment_status_cd FROM Payment)TAB WHERE RNO<2" +
                                                    "and payment_status_cd in ('Capture_ready','Settlement_Completed','Posted','Posting_Requested','Capture_requested')";
			System.out.println("Query ="+Query);
			ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query);
			System.out.println("rset = "+rset);
			while (rset.next()) {
				
				latest_PaymentID = rset.getString("PAYMENT_ID");
			}		
			System.out.println(latest_PaymentID);
			enter(EPP_SearchPageFactory.EnterSearchValue,latest_PaymentID);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		case "Billing Account":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Serial Number":
			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Business Name":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Payment Verification Id":
			String Payment_VerificationId = null;
			String Query_PMTVerification = "SELECT RNO,PMT_VERIFICATION_REFERENCE_VAL,Payment_status_cd FROM(SELECT ROW_NUMBER() OVER (ORDER BY created_Dttm desc) AS RNO,PMT_VERIFICATION_REFERENCE_VAL,Payment_status_cd FROM Payment)TAB WHERE RNO<10" +
                                                    "and payment_status_cd in ('Capture_ready','Settlement_Completed','Posted','Posting_Requested','Capture_requested') and PMT_VERIFICATION_REFERENCE_VAL<> NULL";
			System.out.println("Query ="+Query_PMTVerification);
			ResultSet rset_PMTVerification = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query_PMTVerification);
			System.out.println("rset = "+rset_PMTVerification);
			while (rset_PMTVerification.next()) {
				Payment_VerificationId  = rset_PMTVerification.getString("PMT_VERIFICATION_REFERENCE_VAL");
			}		
			System.out.println(Payment_VerificationId );

			enter(EPP_SearchPageFactory.EnterSearchValue,Payment_VerificationId);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "ePay Payment Confirmation":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
			
		case "Recurring Wallet":

			enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;

		default :

			System.out.println("PLEASE SELECT SEARCH TYPE");
		}

	}
	
	public static void selectSearchTypeforBART() throws SQLException, ClassNotFoundException
	{
		selectOptionByVisibleText(EPP_SearchPageFactory.SelectSearchType,GlobalVariables.Ex_SearchType);
		switch (GlobalVariables.Ex_SearchType) {

		case "Confirmation Number":
			String latest_PaymentID = null;
			String Query = "SELECT RNO,Payment_ID,Payment_status_cd FROM(SELECT ROW_NUMBER() OVER" +
                           "(ORDER BY created_Dttm desc) AS RNO,Payment_ID,Payment_status_cd,BILLING_APPLICATION_CD FROM Payment)TAB WHERE RNO<200 and BILLING_APPLICATION_CD like '%BART%'"+
                           "and payment_status_cd in ('Capture_ready','Settlement_Completed','Posted','Posting_Requested','Capture_requested')";
			System.out.println("Query = "+Query);
			ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","EPWF_APP","epwf_app_epwfst1",Query);
			think(10);
			System.out.println("rset = "+rset);
			while (rset.next()) {
				think(5);
				latest_PaymentID = rset.getString("PAYMENT_ID");
			}		
			System.out.println(latest_PaymentID);
			enter(EPP_SearchPageFactory.EnterSearchValue,latest_PaymentID);
			clear(EPP_SearchPageFactory.FromDate);
			enter(EPP_SearchPageFactory.FromDate, GlobalVariables.Ex_FromDate);
			clear(EPP_SearchPageFactory.ToDate);
			enter(EPP_SearchPageFactory.ToDate, GlobalVariables.Ex_ToDate);
			click(EPP_SearchPageFactory.clickOnSearchNowButton);
			break;
	default :

			System.out.println("PLEASE SELECT SEARCH TYPE");
		}

	}
			public static void ValidateSearch() throws Exception{
	        String Search_errormessage = null;
	        Search_errormessage =  EPP_SearchPageFactory.SearchResultErrorText.getText();	        
	      //  System.out.println("Status of PaymentID = "+Search_errormessage);		
	        if(EPP_SearchPageFactory.SearchResults.isDisplayed()){
		    	System.out.println("Search result is displayed");
		    }else if(EPP_SearchPageFactory.SearchResultErrorText.isDisplayed()){
		    	System.out.println("Last name search failed with error" + EPP_SearchPageFactory.SearchResultErrorText.getText() );
	}
	
}

          

	//DB validation for Searched PaymentID	
	public static void DBVerification() throws Exception{
		String PaymentID = EPP_SearchPageFactory.PaymentID.getText();
		System.out.println("PaymentID = "+PaymentID);
		String result= null;
		String Query = "Select PAYMENT_STATUS_CD from payment where payment_id in "+PaymentID;
		ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","epwf_app", "epwf_app_epwfst1", Query);
		while (rset.next()) {
			result =rset.getString("PAYMENT_STATUS_CD");
		}
		System.out.println("Status of PaymentID = "+result);
		
		System.out.println("PASSED");
	}
	/*public static void selectSearchvalue()
	{
		enter(EPP_SearchPageFactory.EnterSearchValue,GlobalVariables.Ex_SearchValue);     
}*/
	
	//DB validation for refund ID
    public static void VerifyRefundID() throws Exception{
          String RefundMsg = EPP_SearchPageFactory.RefundMessage.getText();
          String[] arrSplit = RefundMsg.split("id is ");
          String RefundID = arrSplit[1];
          System.out.println("RefundID = "+RefundID);
          String result= null;
          String Query = "Select PAYMENT_STATUS_CD from payment where payment_id in "+RefundID;
          ResultSet rset = DatabaseConn.OpenDBConnection("jdbc:oracle:thin:@epwfst1db12c.test.intranet:1602:epwfst1","epwf_app", "epwf_app_epwfst1", Query);
          while (rset.next()) {
                result =rset.getString("PAYMENT_STATUS_CD");
          }
          System.out.println("Status of PaymentID = "+result);
          
          System.out.println("PASSED");
    }
    
       
   }
