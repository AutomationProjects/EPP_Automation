package com.EPP_Automation.Common;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.EPP_Automation.BusinessSpecific.ExcelOperations;
import com.EPP_Automation.BusinessSpecific.GlobalVariables;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;



public class CommonClass {
	ExcelUtils excelUtils = new ExcelUtils();
	
	// Get the current system datetime
	public String getCurrentDateTime(String requiredDateFormat) {
		DateFormat dateFormat = new SimpleDateFormat(requiredDateFormat);
		Calendar cal = Calendar.getInstance();
		String CurrDateTime = dateFormat.format(cal.getTime());
		return CurrDateTime;
	}
		
	
	//JDBC Connectivity
	/*public static ResultSet queryResultFromDB(String query,String dbConnectionStringValue,String dbUserNameValue,String dbPasswordValue) throws SQLException{
		Connection conn =DriverManager.getConnection(dbConnectionStringValue, dbUserNameValue, dbPasswordValue);
		PreparedStatement Prepstmt = conn.prepareStatement(query);
		ResultSet queryResult = Prepstmt.executeQuery();
		return queryResult;
	}*/
	
	 public static ResultSet run()
	 {
		// Open a connection
		 
	      try {
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@epwfst1db.dev.qintra.com:1602:epwfst1", "EPWF_APP", "epwf_app_epwfst1");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	      
	 }
	
	//Create a Random number with given length
	public String getRandomNumber(int length) {
		Random rnd = new Random();
	    long nextLong = Math.abs(rnd.nextLong());
	    return String.valueOf(nextLong).substring(0, length);
	}
	
    public static String getAmountToBePaid(){
        String number="";
        String randomInteger="";
        Random rnd = new Random();
      float nextFloat = Math.abs(rnd.nextFloat());
      long nextLong = Math.abs(rnd.nextLong());
      randomInteger=String.valueOf(nextLong).substring(0, 1);
      number=String.valueOf(Integer.parseInt(randomInteger)+nextFloat);
      return number.substring(0,5);
  }

	
	//Add List into String
	public String addListIntoString(ArrayList<String> list){
		String stringToBeReturned = "";
		for(String itr : list){
			stringToBeReturned += itr +"\n";
		}
		
		return stringToBeReturned;
		
	}


	// Take the assert values from Excel and split them
	public List<String> addAssertValueToArrayList(String sheetName, int rowNumber,String AssertName)
			throws Exception {
		int colNumber;
		String assertUnsplittedString;
		colNumber = excelUtils.getColumnNo(sheetName,AssertName);
		assertUnsplittedString = excelUtils.getCellData(rowNumber, colNumber);
		List<String> assertValues = new ArrayList<String>();
		assertValues = Arrays.asList(assertUnsplittedString
				.split(","));
		return assertValues;
	}
	
	// Common methods for feature file
	public static void data(String data) throws Exception{
		ExcelOperations opr =  new ExcelOperations();
		ExcelUtils excelUtils = new ExcelUtils();
		File testDataExcelFile = GlobalVariables.ExcelFilePath;
		if(data.equalsIgnoreCase("InRegion_EPP_SubmitPayment_ACH_CRIS")){
		int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
		opr.setVariablesFromExcelRCSheet(
				GlobalVariables.RC_SheetName, testDataExcelFile,
				row);
		}else if(data.equalsIgnoreCase("InRegion_EPP_SubmitPayment_CARD_CRIS")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("OutOfRegion_EPP_SubmitPayment_ACH_LATIS")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("OutOfRegion_EPP_SubmitPayment_CARD_LATIS")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("EPP_Multipayment_ACH")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}else if(data.equalsIgnoreCase("EPP_Multipayment_CARD")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("InRegion_RecurringPaymentOptions")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("Search")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("Refund")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("AccountLookup_IR")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("AccountLookup_OOR")){
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		}
		else if(data.equalsIgnoreCase("EPP_Search by_Billing Account_CRIS")) 
{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
}
		else if(data.equalsIgnoreCase("EPP_Search by_Billing Account_LATIS")) 
		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
		}
		else if(data.equalsIgnoreCase("EPP_Search by_Billing Account_BART")) 
		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
		}
		else if(data.equalsIgnoreCase("EPP_Search by_Confirmation Number"))
{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
	}
		else if(data.equalsIgnoreCase("EPP_Search by_Order Number"))

		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
			}
		else if(data.equalsIgnoreCase("EPP_Search by_Serial Number"))


		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
			}
		else if(data.equalsIgnoreCase("EPP_Search by_Customer Last Name"))


		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
			}
		else if(data.equalsIgnoreCase("EPP_Search by_Business Name"))


		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
			}
		else if(data.equalsIgnoreCase("EPP_Search by_Payment Verification Id"))


		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
			}
		else if(data.equalsIgnoreCase("EPP_Search by_ePay Payment Confirmation"))  


		{
					int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
					opr.setVariablesFromExcelRCSheet(
							GlobalVariables.RC_SheetName, testDataExcelFile,
							row);
			}
	
	else if(data.equalsIgnoreCase("EPP_Search by_Recurring Wallet")) 
 

	{
				int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
				opr.setVariablesFromExcelRCSheet(
						GlobalVariables.RC_SheetName, testDataExcelFile,
						row);
		}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_CRIS")) 

		{
				int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
				opr.setVariablesFromExcelRCSheet(
						GlobalVariables.RC_SheetName, testDataExcelFile,
						row);
		}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_ENS")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
	}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_LATIS")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
	}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_AVS")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_BART")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_NIBS")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("EPP_Payment_Option_as_PPP")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("InRegion_New Service Deposit")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("BART_SubmitPayment_ACH_Scheduled")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("BART_MultiInvoice_ACH")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
		
	else if(data.equalsIgnoreCase("BART_SubmitPayment_ACH_Scheduled")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("BART_SubmitPayment_ACH")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	else if(data.equalsIgnoreCase("BART_Refund")) 

	{
			int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
			
	}
	
	else if(data.equalsIgnoreCase("EPP_CARD_SubmitPayment_CRIS_OneTime_Combo")) 

	{	int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);			
}	
	else if(data.equalsIgnoreCase("EPP_ACH_SubmitPayment_CRIS_OneTime_Combo")) 

	{	int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);			
}
	else if(data.equalsIgnoreCase("EPP_CARD_SubmitPayment_LATIS_Scheduled")) 

	{	int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
}	
	else if(data.equalsIgnoreCase("EPP_ACH_SubmitPayment_LATIS_Scheduled")) 

	{	int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);			
}
	else if(data.equalsIgnoreCase("OutRegion_EPP_Refund_CARD_LATIS")) 

	{	int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		//	String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumber.getText();
		//	opr.setConfirmationNumberData(confirmationNumber, row, GlobalVariables.RC_ConfirmationNo, GlobalVariables.RC_SheetName, data);
}
	else if(data.equalsIgnoreCase("EPP_Validate_Max_amount_for_Consumer")) 

	{	int row = excelUtils.getRowNo(testDataExcelFile, GlobalVariables.RC_SheetName, data);
			opr.setVariablesFromExcelRCSheet(
					GlobalVariables.RC_SheetName, testDataExcelFile,
					row);
		//	String confirmationNumber = EPP_NegotiatePaymentPageFactory.confirmationNumber.getText();
		//	opr.setConfirmationNumberData(confirmationNumber, row, GlobalVariables.RC_ConfirmationNo, GlobalVariables.RC_SheetName, data);
}
		
}
}
