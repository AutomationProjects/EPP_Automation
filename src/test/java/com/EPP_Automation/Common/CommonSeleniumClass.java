package com.EPP_Automation.Common;
import java.awt.Robot;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.EPP_Automation.PageFactory.EPP_HomePageFactory;
import com.EPP_Automation.PageFactory.EPP_LoginLogoutPageFactory;
import com.EPP_Automation.PageFactory.EPP_NegotiatePaymentPageFactory;
import com.EPP_Automation.PageFactory.EPP_SearchPageFactory;
import com.EPP_Automation.PageFactory.EPP_TransactionInformationPageFactory;
import com.EPP_Automation.PageFactory.PaymentOptionPageFactory;
import com.EPP_Automation.PageFactory.RecurringPaymentsPageFactory;

public class CommonSeleniumClass {
	public static WebDriver driver;

	public WebDriver getDriver(){
		driver = null;
		return driver;
	}
	// select the appropriate driver
	public static WebDriver selectRequiredBrowser(String browserName) {
		System.out.println(browserName);
		switch (browserName) {
		case "InternetExplorer":

			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer(); 
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true); 
			System.setProperty("webdriver.ie.driver","C:\\Users\\ab77502\\Videos\\EPP framework git upload\\Jeny\\EPP_Automation_Latest\\EPP_Automation_Latest\\src\\test\\java\\com\\EPP_Automation\\Drivers\\IEDriverServer.exe"); 
			driver = new InternetExplorerDriver();
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			return driver;




			//			System.setProperty("webdriver.ie.driver","C:\\IEDriverServer.exe");
			//			driver = new InternetExplorerDriver();
			//			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			//			return driver;

		case "FireFox":
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			return driver;
			
		case "Chrome":
			System.setProperty("webdriver.chrome.driver","C:\\Users\\ab77502\\Videos\\EPP framework git upload\\Jeny\\EPP_Automation_Latest\\EPP_Automation_Latest\\src\\test\\java\\com\\EPP_Automation\\Drivers\\chromedriver.exe"); 
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);

		default:
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			return driver;

		}
	}

	// Maximize window
	public static void maximizeWindow(){

		driver.manage().window().maximize();
	}
	// handle frame ab87891
	public static void framehandle(){

		driver.switchTo().frame(0);
	}

	// Explicit wait
	public boolean CheckElementVisibility(WebElement dynamicElement,
			WebDriver driver) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(dynamicElement));
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	public static void think(long seconds){
		try{
			long milliseconds = seconds*1000;
			Thread.sleep(milliseconds);

		}catch(Exception e){
			System.out.println(e.getMessage());			
		}		
	}

	// Navigate to a certain URL
	public static void navigate(String url) {
		driver.get(url);
	}

	// Enter a specific data in text field
	public static void enter(WebElement element, String testData) {
		element.clear();
		System.out.println("testData:" +testData);
		element.sendKeys(testData);
	}
	// Click on a specific element
	protected static void click(WebElement element) {
		element.click();
	}

	// Switch to a frame by web element
	protected static void switchToFrameByElement(WebElement element) {
		driver.switchTo().frame(element);
   int totalframe=driver.findElements(By.tagName("iframe")).size();
   System.out.println("no of frame"+totalframe);
	}

	// Switch from frame to default content
	protected void switchToDefaultContent(WebDriver driver) {
		driver.switchTo().defaultContent();
	}

	public static void pressCONTROLKeyEvent() throws AWTException
	{
		Robot rb=new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
	}
	public static void rleaseCONTROLKeyEvent() throws AWTException
	{
		Robot rb=new Robot();
		rb.keyRelease(KeyEvent.VK_CONTROL);
	}
	
	public static void KeyPressUParrow() throws AWTException
	{
		Robot rb=new Robot();
		rb.keyPress(KeyEvent.VK_PAGE_UP);
		rb.keyRelease(KeyEvent.VK_PAGE_UP);
	}
	
	
	public static void KeyPressDOWNarrow() throws AWTException
	{
		Robot rb=new Robot();
		rb.keyPress(KeyEvent.VK_PAGE_DOWN);
		rb.keyRelease(KeyEvent.VK_PAGE_DOWN);
	}
	
	
	// Mouse action to move to an element
	protected void mouseActionMoveToElement(WebDriver driver, WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

	// Get the text for an element
	protected static String getText(WebElement element) {
		String text = "";
		text = element.getText();
		return text;
	}

	// Open the link in a new window
	protected void mouseOverAndOpenInNewWindow(WebElement element,
			WebDriver driver) {
		Actions action = new Actions(driver);
		action.moveToElement(element).sendKeys(Keys.SHIFT).click().build()
		.perform();
	}

	// Close the current window
	protected void closeCurrentWindow(WebDriver driver) {
		driver.close();
	}

	// Navigate back in browser
	protected void navigateBack(WebDriver driver) {
		driver.navigate().back();
	}

	// Navigate forward in browser
	protected void navigateForward(WebDriver driver) {
		driver.navigate().forward();
	}

	// End the process of a browser
	protected void quit(WebDriver driver) {
		driver.quit();
	}

	// Reload the current page
	protected void reloadCurrentPage(WebDriver driver) {
		driver.navigate().refresh();
	}

	// get the title of the current page
	protected String getTitle(WebDriver driver) {
		String title = null;
		title = driver.getTitle();
		return title;
	}

	// maximize the current window
	protected void maximizeWindow(WebDriver driver) {
		driver.manage().window().maximize();
	}

	// right click on an element
	protected void rightclick(WebElement element, WebDriver driver) {
		Actions rightClickAction = new Actions(driver);
		rightClickAction.contextClick(element).build().perform();
	}

	// double click on an element
	protected void doubleClick(WebElement element, WebDriver driver) {
		Actions doubleClickAction = new Actions(driver);
		doubleClickAction.doubleClick(element).build().perform();
	}

	// get the attribute value of the element
	protected static String getAttribute(WebElement element, String attributeName) {
		String attributeValue = element.getAttribute(attributeName);
		return attributeValue;
	}

	// get the tag name of the element
	protected String getTagName(WebElement element) {
		return element.getTagName();
	}

	// select DropDown by visible text
	protected static void selectOptionByVisibleText(WebElement element, String text) {

		System.out.println("text:"+text);
		Select select = new Select(element);
		select.selectByVisibleText(text);
	}

	// select DropDown by value
	protected void selectOptionByValue(WebElement element, String value) {
		Select select = new Select(element);
		select.selectByValue(value);
	}

	// select DropDown by index
	protected void selectOptionByIndex(WebElement element, int index) {
		Select select = new Select(element);
		select.selectByIndex(index);
	}

	// accept an alert
	public static void acceptAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		alert.getText();
	}

	// dismiss an alert
	public static void dismissAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	// PageFactory initiate
	public static void initiatePageFactory() {
		PageFactory.initElements(driver, EPP_LoginLogoutPageFactory.class);	
		PageFactory.initElements(driver, EPP_HomePageFactory.class);
		PageFactory.initElements(driver, EPP_TransactionInformationPageFactory.class);
		PageFactory.initElements(driver, EPP_NegotiatePaymentPageFactory.class);
		PageFactory.initElements(driver, EPP_SearchPageFactory.class);
		PageFactory.initElements(driver, PaymentOptionPageFactory.class);

	}

	// close all windows
	public static void closeAllWindows(){
		try{
			Set<String> AllWindows = driver.getWindowHandles();
			for (String window : AllWindows) {
				driver.switchTo().window(window);
				driver.close();
			}
		}catch(NoSuchWindowException e){
			System.out.println(e.getMessage());

		}catch(WebDriverException e){
			System.out.println(e.getMessage());
		}
	}

	public static void clear(WebElement element){
		element.clear();
	}
	//ab87891 Alert 
	public static void alert(){
		Alert alert = driver.switchTo().alert();
		alert.getText();
		alert.accept();
		System.out.println("The text on the alert box is" +  alert);
	}
}
